; DRAM data retention test for Dragon 32/64 machines
; S.Orchard 2021
;
; A pattern is written to memory then refresh is disabled by
; switching to fast mpu rate for a set time.
; After the test time has elapsed, the rate is set to normal
; again and the pattern checked for changes.
; The number of failed addresses are counted and displayed.
;
; The program protects itself from corruption by only being
; resident in memory rows that are refreshed by DRAM accesses
; during the test. The program moves itself around in memory
; to ensure that all memory is tested.
;
; The are four different test patterns and two possible
; program locations meaning that the test should be run at
; least eight times to get full coverage.
;
; A memory size setting allows both 32K and 64K machines to
; be tested. The initial setting is auto-detected.
; The test time can be adjusted to find how long the memory
; can last without refresh.
;
; The program has a bonus feature of displaying info on
; the text screen while the machine is in fast mpu rate.
; This is done by ensuring the mpu accesses the right data
; at the right times to generate a stable display.
;
; Setup screen keys:
;
; 1234/QWER  Adjust time digits up/down
; M toggle memory size between $8000 and $FF00 bytes
; Z zero the max fails counter
; S start one test cycle
; C perform continuous test cycles
; BREAK quit to BASIC
;
; Any key during a test will return to the setup screen
;
;
;
; Assemble with asm6809 http://www.6809.org.uk/asm6809/
;
; Create a DragonDos binary:
;   asm6809 -D -o REFTEST.BIN reftest.s
;
; Create a cartridge rom binary:
;   asm6809 -d PADROM -o reftest.rom reftest.s
;
; Create a CoCo binary:
;   asm6809 -C -d COCO -o reftest.bin reftest.s
;


; -------------------------------------
; define vdg values for some characters
CHR_0      equ ('0)+64
CHR_9      equ ('9)+64
CHR_A      equ 'A
CHR_MINUS  equ ('-)+64

; -------------------------------------
; macro to bodge an assert function
assert	macro
    if !(\1)
        tst __\2__
    endif
 endm

; macro to check that we are not overflowing a
; 64 byte boundary within a 256 byte page
assert64 macro
    assert (* & 255) <= 64,"\1 longer than 64 bytes"
 endm

; -------------------------------------

        org $1000

        leay packed_end,pcr     ; unpack program
        ldx #program_end-256    ;
2       ldb ,-y                 ; length-1
1       lda ,-y                 ;
        sta b,x                 ;
        decb                    ;
        bpl 1b                  ;
        leax -256,x             ;
        cmpx #program_start     ;
        bhs 2b                  ;

        jmp program_start
        
; -------------------------------------
; macros that look after placing code in memory
;   code blocks are logically aligned to 256 byte boundaries
;   and restricted to 64 bytes max. To reduce the file size and
;   load time, the code blocks are packed together with length bytes
;   so that they can be unpacked to the right places on program start.

align_code macro

run_length  set  * - run_start
packed_pos  set  packed_pos + run_length + 1

        fcb run_length-1
        align 256

run_start  set *

        put packed_pos
    endm


; first alignment is a special case
align_code_init macro

packed_pos  set *

        align 256

run_start  set *

        put packed_pos
    endm

; -------------------------------------

packed_start    equ *

        align_code_init

program_start
        orcc #$50
        lds #program_start-192  ; put stack in same rows as code
        clr 113                 ; force cold start on reset
        sta $ffdf               ; map1
        lda #$35                ; enable vsync irq
        sta $ff03               ; (in case of autostart cart)

        leax mem_size_chk,pcr   ; detect memory size
        leay $8000,x            ;
        ldd ,y                  ; try to change the value in upper memory
        coma                    ;
        comb                    ;
        std ,y                  ;
        cmpd ,y                 ;
        bne 1f                  ; 32K or upper memory faulty
        coma                    ; now store a different value in lower memory
        comb                    ; and check if it changes upper memory
        std ,x                  ;
        coma                    ;
        comb                    ;
        cmpd ,y                 ;
        bne 1f                  ; 32K or upper memory faulty
        lda #$ff                ; change memory size to $ff00
        sta end_address,pcr     ;
       
1       lbra mloop

        assert64 "program init"

; -------------------------------------

        align_code

mloop
        lbsr main_screen
        lda loopflag,pcr
        beq 1f

        lbsr pause_1s
        bmi continuous_test       ; no key pressed

1       lbsr wait_no_key
        lbsr wait_key
        lbra key_decode

OP_CLRA equ $4f

continuous_test
        lda #OP_CLRA
start_test equ *-1
        sta loopflag,pcr
        lbsr copy_info
        lbsr clear_screen

        lda ptn_num,pcr
        adda #2
        anda #7
        sta ptn_num,pcr
        bne 1f
        lbsr relocate

1       lbsr do_test

        bra mloop

        assert64 "main loop"

; -------------------------------------

        align_code

copy_info
        ldx #TEST_TIME_POS            ; scrape info from text screen
        leay refresh_64_rows+19,pcr   ; and put on high speed screen
        bsr 10f                       ;
        ldx #MAXCOUNT_POS
        leay 28,y
10      bsr 11f
11      ldd ,x++
        sta ,y++
        stb ,y++
        rts
        
do_test
        lbsr ptn_write
        lbsr wait_no_key
        lbsr no_refresh_test
        lbra ptn_check

key_decode
        leau keytab,pcr
1       lda ,u+
        lbeq mloop      ; no match found
        pulu b,x
        sta $ff02
        bitb $ff00
        bne 1b
        ldd code_offset,pcr
        jmp d,x

        assert64 "key decode"

; -------------------------------------

        align_code

; 4 bytes per key def: $ff02 val, $ff00 val, relative jump to handler
; optionally select CoCo mapping
keytab
  if !COCO
; Dragon key mapping
        fdb $fd01, key_1            ; 1 adj 10s up
        fdb $fd10, key_q            ; Q adj 10s down
        fdb $fb01, key_2            ; 2 adj 1s up
        fdb $7f10, key_w            ; W adj 1s down
        fdb $f701, key_3            ; 3 adj 0.1s up
        fdb $df04, key_e            ; E adj 0.1s down
        fdb $ef01, key_4            ; 4 adj 0.01s up
        fdb $fb10, key_r            ; R adj 0.01s down
        fdb $df08, key_m            ; M toggle mem size
        fdb $fb20, key_z            ; Z zero fails count
        fdb $f710, start_test       ; S start test
        fdb $f704, continuous_test  ; C continuous test
        fdb $fb40, key_brk          ; BREAK quit to BASIC
  else
; CoCo key mapping
        fdb $fd10, key_1            ; 1 adj 10s up
        fdb $fd04, key_q            ; Q adj 10s down
        fdb $fb10, key_2            ; 2 adj 1s up
        fdb $7f04, key_w            ; W adj 1s down
        fdb $f710, key_3            ; 3 adj 0.1s up
        fdb $df01, key_e            ; E adj 0.1s down
        fdb $ef10, key_4            ; 4 adj 0.01s up
        fdb $fb04, key_r            ; R adj 0.01s down
        fdb $df02, key_m            ; M toggle mem size
        fdb $fb08, key_z            ; Z zero fails count
        fdb $f704, start_test       ; S start test
        fdb $f701, continuous_test  ; C continuous test
        fdb $fb40, key_brk          ; BREAK quit to BASIC
  endif
        fcb 0

        assert64 "key table"

; -------------------------------------

        align_code

; test time adjusted by keys 1,2,3,4,q,w,e,r
key_1   ldd #$1000
        bra 1f
key_q   ldd #$9000
        bra 1f
key_2   ldd #$0100
        bra 1f
key_w   ldd #$9900
        bra 1f
key_3   ldd #$0010
        bra 1f
key_e   ldd #$9990
        bra 1f
key_4   ldd #$0001
        bra 1f       
key_r   ldd #$9999
1       leau delay_bcd,pcr
        lbsr bcd_add_word
        lbra mloop

; toggle mem size 8000/ff00
key_m   lda end_address,pcr
        eora #$7f
        sta end_address,pcr
        lbra mloop

        assert64 "key handlers"

; -------------------------------------

        align_code

; zero max fails count
key_z   clra
        clrb
        std maxcount,pcr
        lbra mloop
        
; quit to basic
key_brk sta $ffde       ; map0
        jmp [$fffe]     ; reset

        assert64 "key handlers 2"

; -------------------------------------

        align_code

main_screen
        bsr clear_screen
        ldu #$3ff
        leax text,pcr

1       lda ,x+         ; position offset
        leau a,u
        bne 2f          ; valid offset - output text
        ldb ,x+         ; else get source offset
        beq 1f          ; end of text - return
        abx
        bra 1b
2       lda ,x+
        beq 1b
        sta ,u+
        bra 2b

1       lbra display_info


clear_screen
        ldx #$400
        ldd #$6060
1       std ,x++
        cmpx #$600
        blo 1b
        rts

        assert64 "main screen"

; -------------------------------------

deftext macro
        fcb (1b-2b+1)+\1
1       fcv \2,0
2
        endm

deftxti macro
        fcb (1b-2b+1)+\1
1       fci \2,0
2
        endm

; -------------------------------------

        align_code

text
1
2
        deftext $00,"DRAM RETENTION TEST THING"
        deftext $20,"S.ORCHARD 2021"
        deftext $40,"TEST TIME MS"

        fcb 0,10f-2-*

        assert64 "text"

; -------------------------------------

        align_code

10      deftext $20,"MEMORY SIZE"
        deftext $40,"LAST TEST:"
        deftext $0b,"FAILS"
        deftext $0b,"MAX"
        deftext $2b,"TEST PATTERN"
        deftext $13,"ROWS"

        fcb 0,10f-2-*

        assert64 "text2"

; -------------------------------------

        align_code

10      deftext $0d,"FAILED VALUE"
        deftext $3f,"KEYS:"
        deftxti $21,"1234QWER"
        deftext $09,"ADJUST TIME UP/DOWN"

        fcb 0,10f-2-*

        assert64 "text3"

; -------------------------------------

        align_code

10      deftext $17,"m MEMORY SIZE  z ZERO MAX"
        deftext $20,"s START  c CONTINUOUS TEST"

        fcb 0,10f-2-*

        assert64 "text4"

; -------------------------------------

        align_code

10      deftext $20,"brk QUIT TO BASIC"

        fcb 0,0

        assert64 "text5"

; -------------------------------------

        align_code

TEST_TIME_POS equ $46d
MAXCOUNT_POS  equ $4da

display_info
        ldx #TEST_TIME_POS+4     ; add zero to end of test time
        lda #CHR_0
        sta ,x

        leay bcd_info_table,pcr
1       ldu ,y++
        beq 2f
        ldd code_offset,pcr
        leau d,u
        stu -2,s
        ldu ,y++
        leau d,u
        ldx ,y++
        jsr [-2,s]
        bra 1b
2       rts

disp_rows
        lda ,u
        adda #$40
        sta -4,s
        adda #$bf
        sta -3,s
        leau -4,s
        lbsr hex8_display
        lda #CHR_MINUS
        sta ,x+
        lbra hex8_display

        assert64 "display info"
; -------------------------------------
 
        align_code

disp_failed_value
        ldd failcount,pcr
        lbne hex16_display
        lda #CHR_MINUS
        sta ,x
        rts

; info field display table
; display routine, display data, screen position
bcd_info_table
        fdb bcd_display,       delay_bcd,    TEST_TIME_POS
        fdb hex16_display,     end_address,  TEST_TIME_POS+$21
        fdb bcd_display,       failcount,    $4d1
        fdb bcd_display,       maxcount,     MAXCOUNT_POS
        fdb hex16_display,     pattern,      $50e
        fdb disp_rows,         code_pos1+1,  $519
        fdb disp_failed_value, failed_value, $52e
        fdb 0

        assert64 "display info table"

; -------------------------------------
; perform the delay part of the test.
; syncs to the display, switches to fast mpu rate and
; performs 64 row refresh while generating a display
; for the specified time.
; (SAM doesn't supply video data or refresh in fast mpu rate)

        align_code

no_refresh_test

        lbsr update_delay_count
        leay ,x ;ldy delay_count,pcr

        lda $ff02       ; wait for vsync
        sync            ;
        sta $ffd9       ; set fast mpu rate (no video or refresh)

        lda #19         ; initial alignment
1       deca            ;
        bne 1b          ;
        tfr a,a         ;
        
1       ldx #157                ; 3      (x=157 gives approx 10ms)
        
2       exg a,a                       ; 8
        lbsr refresh_64_rows          ; 9+67 = 76
        leax -1,x                     ; 5
        beq 3f                        ; 3 (92)
        lda $ff00                     ; 5
        anda #127                     ; 2
        inca                          ; 2
        bpl 5f                        ; 3
        leau ,u++                     ; 7
        bra 2b                        ; 3 (114)
        
3       com 50f,pcr             ; 11
        leay -1,y               ; 5
        bne 1b                  ; 3 (114)

4       lbra slow_mpu_rate

5       clr loopflag,pcr
        bra 4b

        assert64 "outer refresh loop"

; -------------------------------------
; refresh 64 rows
; while outputting a line of vdg data

        align_code

refresh_64_rows
   
        lda #96
        lda #'T
        lda #'E
        lda #'S
        lda #'T
        lda #'I
        lda #'N
        lda #'G
        lda #96
        lda #CHR_0
        lda #CHR_0
        lda #CHR_0
        lda #CHR_0
        lda #CHR_0
        lda #'M
        lda #'S
        lda #96
        lda #'M
        lda #'A
        lda #'X
        lda #96
        lda #'F
        lda #'A
        lda #'I
        lda #'L
        lda #'S
        lda #96
        lda #CHR_0
        lda #CHR_0
        lda #CHR_0
        lda #CHR_0      ; 31 x lda # = 62 bytes / 62 cycles
        rts             ; 1 / 5
50      fcb 96          ; final display byte gets refreshed by 6809 fetch during rts decode
        
        assert (* & 255) == 64, "refresh sequence not equal to 64 bytes"
        
; -------------------------------------

        align_code

ptn_write
        leau patterns,pcr
        lda ptn_num,pcr
        ldu a,u
        stu pattern,pcr
        
        clra
        ldb code_pos1+1,pcr
        andb #$40
        eorb #$40
        tfr d,x
        
2       ldb #$c0/2
1       stu ,x++
        decb
        bne 1b
        leax $40,x
        cmpx end_address,pcr
        blo 2b
        rts
        
patterns fdb $ff00,$00ff,$55aa,$aa55

; part of pattern check routine below
ptn_fail
        ldx -2,y
        stx failed_value,pcr
        lda #1
        lbsr bcd_add
        lbra bcd_max

        assert64 "pattern write"

; -------------------------------------

        align_code

ptn_check
        clra
        clrb
        std failcount,pcr
        ldx pattern,pcr
        leau failcount,pcr

        ;clra
        ldb code_pos1+1,pcr
        andb #$40
        eorb #$40
        tfr d,y
        
2       ldb #$c0/2
1       cmpx ,y++
        beq 3f
        pshs x
        lbsr ptn_fail
        puls x
3       decb
        bne 1b

        leay $40,y
        cmpy end_address,pcr
        blo 2b
        ldx ,u
        cmpx maxcount,pcr
        blo 9f
        stx maxcount,pcr
9       rts

        assert64 "pattern check"

; -------------------------------------

        align_code

wait_no_key
1       clr $ff02       ;
        sync            ; debounce delay
        bsr check_keys
        bpl 1b
        rts

check_keys
        lda $ff00       ; 5
        anda #127       ; 2
        inca            ; 2
        rts             ; 5 (14)
        
wait_key
1       clr $ff02       ;
        sync            ; debounce delay
        bsr check_keys
        bmi 1b
        rts

pause_1s        
        ldx #27969      ; 1 second at normal rate
        clr $ff02
1       bsr check_keys  ; 7+14
        bpl 2f          ; 3   key pressed
        leax -1,x       ; 5
        bne 1b          ; 3 (32)
2       rts

        assert64 "keyboard code"

; -------------------------------------

        align_code

bcd_add
        adda 1,u
        daa
        sta 1,u
        lda ,u
        adca #0
        daa
        sta ,u
        rts

bcd_add_word
        adda ,u
        daa
        sta ,u
        tfr b,a
        bra bcd_add

bcd_max
        pshs d
        bcc 1f
        ldd #$9999
        std ,u
1       puls d,pc

bcd_display
        bsr 1f
1       lda ,u
        lsra
        lsra
        lsra
        lsra
        adda #CHR_0
        sta ,x+
        lda ,u+
        anda #15
        adda #CHR_0
        sta ,x+
        rts

        assert64 "bcd functions"

; -------------------------------------

        align_code

hex16_display
        bsr hex8_display
hex8_display
	lda ,u
	lsra
	lsra
	lsra
	lsra
	adda #CHR_0
	cmpa #CHR_9
	bls 1f
	adda #CHR_A-10-CHR_0
1	sta ,x+
	lda ,u+
	anda #15
	adda #CHR_0
	cmpa #CHR_9
	bls 1f
	adda #CHR_A-10-CHR_0
1	sta ,x+
	rts

        assert64 "hex functions"

; -------------------------------------

        align_code

slow_mpu_rate
        sta $ffd7       ; return to normal rate
        sta $ffd8       ; via sam datasheet method
        tst >0          ; (so we don't violate 68B09 timing)
        brn *           ;
        sta $ffd6       ;
        rts

; convert delay count from bcd to integer
update_delay_count
        leau delay_bcd,pcr
        bsr 10f
        lda #100
        mul
        tfr d,x
        bsr 10f
        abx
        cmpx #0                 ; avoid delay=0
        bne 1f                  ;
        leax 1,x                ;
1       stx delay_count,pcr
        rts

10      lda ,u
        lsra
        lsra
        lsra
        lsra
        ldb #10
        mul
        pshs b
        ldb ,u+
        andb #15
        addb ,s+
        rts


        assert64 "misc routines"

; -------------------------------------
; Moves program between rows 0-63 and rows 191-255
; Needs to be called from top level
; (anything else on stack would be pointing to wrong address)

        align_code

relocate
        ldx code_pos1,pcr       ; swap current & next code pointers
        ldy code_pos2,pcr       ;
        stx code_pos2,pcr       ;
        sty code_pos1,pcr       ;

        lda code_offset+1,pcr   ; determine adjustment for code labels
        eora #$c0               ; (before everything gets moved)
        sta code_offset+1,pcr   ;

2       lda #32
1       ldu ,x++
        stu ,y++
        deca
        bne 1b
        leax 192,x
        leay 192,y
        cmpx #program_end
        blo 2b
        
        ldd code_pos1,pcr       ; determine how far code was moved
        subd code_pos2,pcr      ;
        puls x                  ; get return address
        leas d,s                ; move stack
        jmp d,x                 ; return to code in new location
     

code_pos1       fdb program_start       ; current code position
code_pos2       fdb program_start+192   ; next code position
code_offset     fdb 0                   ; offset from original position

        assert64 "relocation code"

; -------------------------------------

        align_code

loopflag        fcb 0
end_address     fdb $8000   ; init mem size 32K
delay_bcd       fdb $500    ; init test time 5s
delay_count     fdb 0
ptn_num         fcb 6       ; advanced to 0 before first test
pattern         fdb 0
failcount       fdb 0
maxcount        fdb 0
failed_value    fdb 0
mem_size_chk    fdb 0       ; used to detect memory size

        assert64 "variables"

; -------------------------------------

        align_code

packed_end      equ packed_pos
program_end     equ *

  ; make rom image a multiple of 256 bytes
  if PADROM
        org packed_end
        align 256,0
  endif
